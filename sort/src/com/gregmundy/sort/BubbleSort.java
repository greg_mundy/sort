package com.gregmundy.sort;

/**
 * Implementation of the bubblesort algorithm.
 * 
 * @author Greg Mundy
 */

public class BubbleSort implements Sort {
    @Override
    public Integer[] sort(Integer[] unsortedList, Integer size) {
	Boolean swapped = false;
	Integer temp = 0;

	do {
	    swapped = false;
	    for (Integer i = 1; i <= size - 1; i++) {
		if (unsortedList[i - 1] > unsortedList[i]) {
		    temp = unsortedList[i - 1];
		    unsortedList[i - 1] = unsortedList[i];
		    unsortedList[i] = temp;
		    swapped = true;
		}
	    }
	} while (swapped);
	return unsortedList;
    }

    @Override
    public String getName() {
	return "Bubble Sort";
    }
}
