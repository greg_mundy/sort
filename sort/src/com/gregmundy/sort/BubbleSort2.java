package com.gregmundy.sort;

/**
 * Has the first optimization of the bubble sort.
 * 
 * @author Macey
 * @version 1
 */
public class BubbleSort2 implements Sort {
    @Override
    public Integer[] sort(Integer[] unsortedList, Integer size) {
	Integer temp = 0;
	Boolean swapped = false;
	Integer n = size;

	do {
	    swapped = false;
	    for (Integer i = 1; i <= n - 1; i++) {
		if (unsortedList[i - 1] > unsortedList[i]) {
		    temp = unsortedList[i - 1];
		    unsortedList[i - 1] = unsortedList[i];
		    unsortedList[i] = temp;
		    swapped = true;
		}
	    }
	    n = n - 1;
	} while (swapped);
	return unsortedList;
    }

    @Override
    public String getName() {
	return "Bubble Sort 2";
    }
}
