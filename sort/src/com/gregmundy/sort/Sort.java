package com.gregmundy.sort;

/**
 * This interface is used for describing the methods needed for sorting
 * algorithms.
 * 
 * @author Greg Mundy
 */
public interface Sort {
    /**
     * Sort an unsorted array of elements.
     * 
     * @param unsortedList
     *            List of unsorted items.
     * @return List of items in a sorted form.
     */
    Integer[] sort(Integer[] unsortedList, Integer size);

    /**
     * Display the name of the algorithm.
     * 
     * @return The algorithm's name.
     */
    String getName();
}
