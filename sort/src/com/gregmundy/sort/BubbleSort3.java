package com.gregmundy.sort;

/**
 * Write a description of class BubbleSort3 here.
 * 
 * @author (your name)
 * @version (a version number or a date)
 */
public class BubbleSort3 implements Sort {
    @Override
    public Integer[] sort(Integer[] unsortedList, Integer size) {
	Boolean swapped = false;
	Integer temp = 0;
	Integer n = size;
	Integer newn = 0;
	do {
	    newn = 0;
	    for (Integer i = 1; i <= n - 1; i++) {
		if (unsortedList[i - 1] > unsortedList[i]) {
		    temp = unsortedList[i - 1];
		    unsortedList[i - 1] = unsortedList[i];
		    unsortedList[i] = temp;
		    newn = i;
		}
	    }
	    n = newn;
	} while (n != 0);
	return unsortedList;
    }

    @Override
    public String getName() {
	return "Bubble Sort 3";
    }
}
