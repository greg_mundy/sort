package com.gregmundy.sort;

/**
 * Implementation of the insertion sort algorithm.
 * 
 * @author Savannah Williams
 * @version 1
 */
public class InsertionSort implements Sort {
    @Override
    public Integer[] sort(Integer[] unsortedList, Integer size) {
	Integer numberToInsert;
	Integer hole;

	for (Integer i = 1; i < size; i++) {
	    numberToInsert = unsortedList[i];
	    hole = i;
	    while (hole > 0 && unsortedList[hole - 1] > numberToInsert) {
		unsortedList[hole] = unsortedList[hole - 1];
		hole--;
	    }
	    unsortedList[hole] = numberToInsert;
	}
	return unsortedList;
    }

    @Override
    public String getName() {
	return "Insertion Sort";
    }
}
