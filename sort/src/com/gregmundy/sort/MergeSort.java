package com.gregmundy.sort;

/**
 * Implementation of the MergeSort algorithm. An algorithm that runs in O(n log
 * n) time.
 * 
 * @author Gregory Mundy
 */
public class MergeSort implements Sort {
    @Override
    public Integer[] sort(Integer[] unsortedList, Integer size) {
	Integer middle = size / 2;
	Integer[] left = new Integer[size];
	Integer[] right = new Integer[size];
	Integer sizeLeft = 0;
	Integer sizeRight = 0;

	if (size <= 1) {
	    return unsortedList;
	}

	for (Integer i = 0; i < middle; i++) {
	    left[sizeLeft] = unsortedList[i];
	    sizeLeft++;
	}

	for (Integer j = middle; j < size; j++) {
	    right[sizeRight] = unsortedList[j];
	    sizeRight++;
	}

	left = sort(left, sizeLeft);
	right = sort(right, sizeRight);

	return merge(left, right, sizeLeft, sizeRight);
    }

    @Override
    public String getName() {
	return "MergeSort";
    }

    private Integer[] merge(Integer[] left, Integer[] right, Integer sizeLeft,
	    Integer sizeRight) {
	Integer[] result = new Integer[sizeLeft + sizeRight];
	Integer currentPosition = 0;
	Integer currentLeftIndex = 0;
	Integer currentRightIndex = 0;

	while (sizeLeft > 0 || sizeRight > 0) {
	    if (sizeLeft > 0 && sizeRight > 0) {
		if (left[currentLeftIndex] <= right[currentRightIndex]) {
		    result[currentPosition] = left[currentLeftIndex];
		    currentPosition++;
		    currentLeftIndex++;
		    sizeLeft--;
		} else {
		    result[currentPosition] = right[currentRightIndex];
		    currentPosition++;
		    currentRightIndex++;
		    sizeRight--;
		}
	    } else if (sizeLeft > 0) {
		result[currentPosition] = left[currentLeftIndex];
		currentPosition++;
		currentLeftIndex++;
		sizeLeft--;
	    } else if (sizeRight > 0) {
		result[currentPosition] = right[currentRightIndex];
		currentPosition++;
		currentRightIndex++;
		sizeRight--;
	    }
	}
	return result;
    }
}
