package com.gregmundy.sort;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * The test class InsertionSortTest.
 *
 * @author Greg Mundy
 */
public class InsertionSortTest {
    @Test
    public void testInsertionSort() {
	Integer[] unsortedList = { 10, 9, 8, 7, 6, 5, 4, 3, 2, 1 };
	InsertionSort insertionSort = new InsertionSort();

	unsortedList = insertionSort.sort(unsortedList, 10);

	System.out.println("The output of the sorted list...");
	for (Integer i = 0; i < 10; i++) {
	    System.out.print(unsortedList[i] + " ");
	    if (i == 9) {
		System.out.println();
	    }
	}

	assertTrue(unsortedList[0] == 1);
	assertTrue(unsortedList[9] == 10);
    }
}
