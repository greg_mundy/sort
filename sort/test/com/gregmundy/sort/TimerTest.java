package com.gregmundy.sort;

/**
 * The test class TimerTest.
 *
 * @author  Greg Mundy
 */
import static org.junit.Assert.assertTrue;

import java.util.Random;

import org.junit.Before;
import org.junit.Test;

public class TimerTest {
    // array sizes
    private static Integer SIZE_1 = 1;
    private static Integer SIZE_2 = 10;
    private static Integer SIZE_3 = 100;
    private static Integer SIZE_4 = 1000;
    private static Integer SIZE_5 = 10000;
    private static Integer SIZE_6 = 100000;
    private static Integer SIZE_7 = 1000000;
    private static Integer SIZE_8 = 10000000;

    // define test arrays
    private Integer[] testCase1 = new Integer[SIZE_1];
    private Integer[] testCase2 = new Integer[SIZE_2];
    private Integer[] testCase3 = new Integer[SIZE_3];
    private Integer[] testCase4 = new Integer[SIZE_4];
    private Integer[] testCase5 = new Integer[SIZE_5];
    private Integer[] testCase6 = new Integer[SIZE_6];
    private Integer[] testCase7 = new Integer[SIZE_7];
    private Integer[] testCase8 = new Integer[SIZE_8];

    private void runTimedSort(Sort algorithm, Integer testCase[], Integer size) {
	System.out.println("[ACTIVITY] Testing " + algorithm.getName()
		+ " with " + size + " elements.");
	Long startTime = System.nanoTime();
	Integer[] sortedArray = algorithm.sort(testCase, size);
	Long endTime = System.nanoTime();
	Double totalTime = (endTime - startTime) / 1000000000D;
	System.out.println("[DONE] Completed in " + totalTime + " seconds.");

	// Be good and test that the array actual returned sorted.
	for (Integer i = 0; i < sortedArray.length - 1; i++) {
	    assertTrue(sortedArray[i] <= sortedArray[i + 1]);
	}
    }

    private Boolean testAlgorithm(Sort algorithm) {
	System.out.println("\t[ASSERT] Validating " + algorithm.getName()
		+ " Algorithm.");
	Integer[] testList = { 10, 9, 8, 7, 6, 5 };
	testList = algorithm.sort(testList, 6);
	Boolean sortWorks = false;

	if (testList != null && testList[0] == 5 && testList[1] == 6
		&& testList[2] == 7 && testList[3] == 8 && testList[4] == 9
		&& testList[5] == 10) {
	    sortWorks = true;
	    System.out.println("\t[SUCCESS] " + algorithm.getName()
		    + " Algorithm Works!");
	    // runTimedSort(algorithm, testCase1, SIZE_1);
	    // runTimedSort(algorithm, testCase2, SIZE_2);
	    // runTimedSort(algorithm, testCase3, SIZE_3);
	    // runTimedSort(algorithm, testCase4, SIZE_4);
	    // runTimedSort(algorithm, testCase5, SIZE_5);
	    // runTimedSort(algorithm, testCase6, SIZE_6);
	    runTimedSort(algorithm, testCase7, SIZE_7);
	    // runTimedSort(algorithm, testCase8, SIZE_8);
	} else {
	    System.out.println("\t[FAILURE] " + algorithm.getName()
		    + " Algorithm Is Invalid.");
	}

	return sortWorks;
    }

    @Before
    public void setUp() {
	Random random = new Random();
	System.out.println("[ACTIVITY] Randomly populating test arrays.");

	// start the timer
	Long startTime = System.nanoTime();

	// array 1
	testCase1[0] = random.nextInt(10) + 1;

	// array 2
	for (Integer i = 0; i < 10; i++) {
	    testCase2[i] = random.nextInt(10) + 1;
	}

	// array 3
	for (Integer i = 0; i < 100; i++) {
	    testCase3[i] = random.nextInt(10) + 1;
	}

	// array 4
	for (Integer i = 0; i < 1000; i++) {
	    testCase4[i] = random.nextInt(10) + 1;
	}

	// array 5
	for (Integer i = 0; i < 10000; i++) {
	    testCase5[i] = random.nextInt(10) + 1;
	}

	// array 6
	for (Integer i = 0; i < 100000; i++) {
	    testCase6[i] = random.nextInt(10) + 1;
	}

	// array 7
	for (Integer i = 0; i < 1000000; i++) {
	    testCase7[i] = random.nextInt(10) + 1;
	}

	// array 8
	for (Integer i = 0; i < 10000000; i++) {
	    testCase8[i] = random.nextInt(10) + 1;
	}

	// End Time
	Long endTime = System.nanoTime();

	Double totalTime = (endTime - startTime) / 1000000000D;

	System.out.println("[DONE] Completed in " + totalTime + " seconds.");
    }

    @Test
    public void testSortingAlgorithms() {
	assertTrue(testAlgorithm(new MergeSort()));
	assertTrue(testAlgorithm(new InsertionSort()));
	assertTrue(testAlgorithm(new BubbleSort()));
    }
}
